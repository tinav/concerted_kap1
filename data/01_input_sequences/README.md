
This folder contains initial fasta sequences separated into `all.5prime.fasta`, `all.3prime.fasta`, and `all.fasta.CDS`
depending on location of the sequences within KAP1 genomic region. File `all.5prime.fasta` contains sequences from 
5' flanking regions, file `all.3prime.fasta` contains sequences from 3' flanking regions, and `all.CDS.fasta` 
contains coding sequences.

The file `all.aa.CDS.fasta` contains coding sequences translated to amino acid sequences.

