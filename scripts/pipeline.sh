#!/bin/bash

REPO_FOLDER="" #path to this repository, edit
PHYML_BIN="" #path to PhyML binaries, edit

SCRIPTS=${REPO_FOLDER}/scripts
DATA=${REPO_FOLDER}/data
BOOTSTRAP=1000

mkdir -p ${DATA}/02_msa_mafft
mkdir -p ${DATA}/03_gblocks/conserved
mkdir -p ${DATA}/03_gblocks/not_so_conserved
mkdir -p ${DATA}/04_synonymous_or_not
mkdir -p ${DATA}/05_phylip/bootstrap-${BOOTSTRAP}/conserved
mkdir -p ${DATA}/05_phylip/bootstrap-${BOOTSTRAP}/not_so_conserved
mkdir -p ${DATA}/05_phylip/bootstrap-${BOOTSTRAP}/synonymous_or_not
mkdir -p ${DATA}/06_trees/bootstrap-${BOOTSTRAP}/conserved
mkdir -p ${DATA}/06_trees/bootstrap-${BOOTSTRAP}/not_so_conserved
mkdir -p ${DATA}/06_trees/bootstrap-${BOOTSTRAP}/synonymous_or_not
mkdir -p ${DATA}/07_final_plots

cd ${SCRIPTS}
source ./utils.sh

###
# 1.
# convert CDS fasta from nucleotides to amino acids,
# so that multiple sequence alignment of CDS sequences
# takes amino acids into account:
###

process_command "perl ${SCRIPTS}/dna_to_aa.pl \
	${DATA}/01_input_sequences/all.CDS.fasta \
	> ${DATA}/01_input_sequences/all.aa.CDS.fasta" \
	0 -

###
# 2.
# generate multiple sequence alignments using mafft:
###

process_command "mafft --nuc --localpair --maxiterate 1000 \
	${DATA}/01_input_sequences/all.3prime.fasta \
	> ${DATA}/02_msa_mafft/msa.nt.3prime.mafftl.fasta" \
	0 -

process_command "mafft --nuc --localpair --maxiterate 1000 \
	${DATA}/01_input_sequences/all.5prime.fasta \
	> ${DATA}/02_msa_mafft/msa.nt.5prime.mafftl.fasta" \
	0 -

process_command "mafft --amino --localpair --maxiterate 1000 \
	${DATA}/01_input_sequences/all.aa.CDS.fasta \
	> ${DATA}/02_msa_mafft/msa.aa.CDS.mafftl.fasta" \
	0 -

###
# 3.
# use http://www.cbs.dtu.dk/services/RevTrans/ to get nucleotide alignment
# from the amino acid one for the CDS sequences.
# Store the output to ${DATA}/02_msa_mafft/msa.nt.CDS.mafftl.RevTrans.fasta
###


###
# 4.
# Use gblocks to identify conserved blocks of the three alignments
# in the folder ${DATA}/02_msa_mafft/ with the following parameter
# settings:
#
# 4.1
# Processed file: msa.nt.5prime.mafftl.fasta
# Number of sequences: 40
# Alignment assumed to be: DNA
# Minimum Number Of Sequences For A Conserved Position: 21
# Minimum Number Of Sequences For A Flanking Position: 31
# Maximum Number Of Contiguous Nonconserved Positions: 15
# Minimum Length Of A Block: 2
# Allowed Gap Positions: With Half
#
# 4.2
# Processed file: msa.nt.CDS.mafftl.RevTrans.fasta
# Number of sequences: 40
# Alignment assumed to be: Codons
# Minimum Number Of Sequences For A Conserved Position: 21
# Minimum Number Of Sequences For A Flanking Position: 34
# Maximum Number Of Contiguous Nonconserved Positions: 2
# Minimum Length Of A Block: 30
# Allowed Gap Positions: None
#
# 4.3
# Processed file: msa.nt.3prime.mafftl.fasta
# Number of sequences: 40
# Alignment assumed to be: DNA
# Minimum Number Of Sequences For A Conserved Position: 21
# Minimum Number Of Sequences For A Flanking Position: 31
# Maximum Number Of Contiguous Nonconserved Positions: 15
# Minimum Length Of A Block: 2
# Allowed Gap Positions: With Half
#
# Store the outputs to the folder ${DATA}/03_gblocks/conserved
###


###
# 5.
# Use gblocks identify _not_so_conserved_ blocks of the three
# alignments in the folder ${DATA}/02_msa_mafft/ with a bit more
# relaxed parameter settings:
#
# 5.1
# Processed file: msa.nt.5prime.mafftl.fasta
# Number of sequences: 40
# Alignment assumed to be: DNA
# Minimum Number Of Sequences For A Conserved Position: 21
# Minimum Number Of Sequences For A Flanking Position: 21
# Maximum Number Of Contiguous Nonconserved Positions: 2000
# Minimum Length Of A Block: 2
# Allowed Gap Positions: With Half
#
# 5.2
# Processed file: msa.nt.CDS.mafftl.RevTrans.fasta
# Number of sequences: 40
# Alignment assumed to be: DNA
# Minimum Number Of Sequences For A Conserved Position: 21
# Minimum Number Of Sequences For A Flanking Position: 21
# Maximum Number Of Contiguous Nonconserved Positions: 2000
# Minimum Length Of A Block: 5
# Allowed Gap Positions: With Half
#
# 5.3
# Processed file: msa.nt.3prime.mafftl.fasta
# Number of sequences: 40
# Alignment assumed to be: DNA
# Minimum Number Of Sequences For A Conserved Position: 21
# Minimum Number Of Sequences For A Flanking Position: 21
# Maximum Number Of Contiguous Nonconserved Positions: 2000
# Minimum Length Of A Block: 2
# Allowed Gap Positions: With Half
#
# Store the outputs to the folder ${DATA}/03_gblocks/not_so_conserved
###


###
# 6.
# Generate fasta files with alignments containing only synonymous
# positions in syn.fasta file and alignments containing only nonsynonymous
# positions in nonsyn.fasta.
#
# The files are located in folder ${DATA}/04_synonymous_or_not
###

process_command "perl split_msa_nonsyn_syn.pl \
	${DATA}/02_msa_mafft/msa.nt.CDS.mafftl.RevTrans.fasta \
	${DATA}/04_synonymous_or_not/nonsyn.fasta \
	${DATA}/04_synonymous_or_not/syn.fasta" \
	0 -


###
# 7.
# Generate phylogenetic trees from the conserved and not_so_conserved
# multiple sequence alignments.
#
# Store output phylogenetic trees and bootstrap trees in the folders
# ${DATA}/05_phylip/bootstrap-${BOOTSTRAP}/(not_so_)conserved
# and store *.phy trees and basic plots in ${DATA}/06_trees/bootstrap-${BOOTSTRAP}/(not_so_)conserved
###

dtypes=(conserved not_so_conserved)

for dtype in ${dtypes[@]}; do

	names=(msa.nt.5prime.mafftl msa.nt.CDS.mafftl.RevTrans msa.nt.3prime.mafftl)

	for name in ${names[@]}; do
		len=`perl ${SCRIPTS}/multiple_fasta_seq_names_and_lengths.pl --fasta ${DATA}/03_gblocks/${dtype}/${name}.fasta | head -1 | cut -f 2`
		echo -e "sequence:\t${name}"
		echo -e "alignment length:\t${len}"

		process_command "bash alignment_to_tree.sh \
				-n ${name} \
				-l ${len} \
				-s ${SCRIPTS} \
				-b ${BOOTSTRAP} \
				-p ${PHYML_BIN} \
				-i ${DATA}/03_gblocks/${dtype} \
				-F YES \
				-P ${DATA}/05_phylip/bootstrap-${BOOTSTRAP}/${dtype} \
				-T ${DATA}/06_trees/bootstrap-${BOOTSTRAP}/${dtype} \
				-M NO " 0 -
	done
done

###
# 8.
# Generate phylogenetic trees from the alignments of synonymous
# and nonsynonymous positions of the coding sequence
#
# Store output phylogenetic trees and bootstrap trees in the folder
# ${DATA}/05_phylip/bootstrap-${BOOTSTRAP}/synonymous_or_not
# and store *.phy trees and basic plots in ${DATA}/06_trees/bootstrap-${BOOTSTRAP}/synonymous_or_not
###

names=(syn nonsyn)

for name in ${names[@]}; do
	len=`perl ${SCRIPTS}/multiple_fasta_seq_names_and_lengths.pl --fasta ${DATA}/04_synonymous_or_not/${name}.fasta | head -1 | cut -f 2`
		echo -e "sequence:\t${name}"
		echo -e "alignment length:\t${len}"

		process_command "bash alignment_to_tree.sh \
				-n ${name} \
				-l ${len} \
				-s ${SCRIPTS} \
				-b ${BOOTSTRAP} \
				-p ${PHYML_BIN} \
				-i ${DATA}/04_synonymous_or_not/ \
				-F YES \
				-P ${DATA}/05_phylip/bootstrap-${BOOTSTRAP}/synonymous_or_not \
				-T ${DATA}/06_trees/bootstrap-${BOOTSTRAP}/synonymous_or_not \
				-M NO " 0 -
	done



###
# 9.
# Adjust the phylogenetic trees for Figure 3 and Figure 5 and store plots
# to ${DATA}/07_final_plots
###

# Figure 3

process_command "R --vanilla --slave --args \
	${DATA}/06_trees/bootstrap-${BOOTSTRAP}/conserved/msa.nt.5prime.mafftl.phy \
	${DATA}/07_final_plots/manuscriptFigure3.5prime.conserved.pdf \
	< format_conserved_5prime_tree.R" 0 -

process_command "R --vanilla --slave --args \
	${DATA}/06_trees/bootstrap-${BOOTSTRAP}/conserved/msa.nt.CDS.mafftl.RevTrans.phy \
	${DATA}/07_final_plots/manuscriptFigure3.CDS.RevTrans.conserved.pdf \
	< format_conserved_cds_tree.R" 0 -

process_command "R --vanilla --slave --args \
	${DATA}/06_trees/bootstrap-${BOOTSTRAP}/conserved/msa.nt.3prime.mafftl.phy \
	${DATA}/07_final_plots/manuscriptFigure3.3prime.conserved.pdf \
	< format_conserved_5prime_tree.R" 0 -

# Figure 5

process_command "R --vanilla --slave --args \
	${DATA}/06_trees/bootstrap-${BOOTSTRAP}/synonymous_or_not/nonsyn.phy \
	${DATA}/07_final_plots/manuscriptFigure5.nonsynonymous.pdf \
	< format_nonsynonymous_tree.R" 0 -

process_command "R --vanilla --slave --args \
	${DATA}/06_trees/bootstrap-${BOOTSTRAP}/synonymous_or_not/syn.phy \
	${DATA}/07_final_plots/manuscriptFigure5.synonymous.pdf \
	< format_synonymous_tree.R" 0 -

###
# 10.
# Copy not_so_conserved tree plots for Figure S3 to ${DATA}/07_final_plots
###

cp ${DATA}/06_trees/bootstrap-${BOOTSTRAP}/not_so_conserved/msa.nt.5prime.mafftl.pdf ${DATA}/07_final_plots/manuscriptFigureS3.5prime.not.so.conserved.pdf
cp ${DATA}/06_trees/bootstrap-${BOOTSTRAP}/not_so_conserved/msa.nt.CDS.mafftl.RevTrans.pdf ${DATA}/07_final_plots/manuscriptFigureS3.CDS.not.so.conserved.pdf
cp ${DATA}/06_trees/bootstrap-${BOOTSTRAP}/not_so_conserved/msa.nt.3prime.mafftl.pdf ${DATA}/07_final_plots/manuscriptFigureS3.3prime.not.so.conserved.pdf
