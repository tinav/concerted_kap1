#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use Bio::AlignIO;
use Bio::SimpleAlign;

my $inFile = shift;
my $outNonSyn = shift;
my $outSyn = shift;

my $str = Bio::AlignIO->new(-file => $inFile, -format => "fasta");
my $aln = $str->next_aln();


my $length = int($aln->length());
print "Alignment length: $length\n";
if ($length % 3 != 0){print "WARNING: alignment length is not divisible by 3!\n"}
else{print "Alignment length is divisible by 3.\n"}

my $syn_positions=();
my @nonsyn_positions=[];

my $nonsyn=$aln;
my $syn=$aln;

for (my $i=$length-1; $i >= 0; $i--){
	print "i: ",$i,"\n";
	if ($i % 3 == 2){
	my $new_nonsyn = $nonsyn->remove_columns([$i,$i]);
	$nonsyn=$new_nonsyn;
	print "NONSYN: remove_columns([$i,$i])\n";
	}elsif($i % 3 == 0){
	my $new_syn = $syn->remove_columns([$i,$i+1]);
	$syn=$new_syn; 
	print "SYN: remove_columns([$i,$i+1])\n";
	}
}

open FH_NONSYN, ">", $outNonSyn or die "out file nonSyn: $outNonSyn";
foreach my $seq ($nonsyn->each_alphabetically()){
  print FH_NONSYN ">", $seq->id, "\n", $seq->seq, "\n";
}
close FH_NONSYN;


open FH_SYN, ">", $outSyn or die "out file syn: $outSyn";
foreach my $seq ($syn->each_alphabetically()){
  print FH_SYN ">", $seq->id, "\n", $seq->seq, "\n";
}
close FH_SYN;
