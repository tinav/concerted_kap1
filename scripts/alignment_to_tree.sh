#!/bin/bash


usage(){
	echo "Usage:"
}

while getopts ":M:n:l:s:b:p:i:F:P:T:h" OPT; do
	case $OPT in
		n) name="$OPTARG"
		;;
		l) len="$OPTARG"
		;;
		s) scriptsFolder="$OPTARG"
		;;
		b) BOOTSTRAP="$OPTARG"
		;;
		p) phymlBin="$OPTARG"
		;;
		i) inFolder="$OPTARG"
		;;
		F) fastaGB="$OPTARG"
		;;
		P) phylipFolder="$OPTARG"
		;;
		T) treeFolder="$OPTARG"
		;;
		M) isMotif="$OPTARG"
		;;
		h) usage
		exit 0
		;;
		\?) #unrecognized option - show usage
		echo -e "\nOption -$OPTARG not allowed.\n"
		usage
		exit 2
		;;
	esac
done

source ./utils.sh

echo "# Script $0 starts: "$(date)
echo 

# print parameters settings
echo "# name=${name}"
echo "# len=${len}"
echo "# folder=${folder}"
echo "# inFolder=${inFolder}"
echo "# BOOTSTRAP=${BOOTSTRAP}"
echo "# phymlBin=${phymlBin}"
echo "# fastaGB=${fastaGB}"
echo "# phylipFolder=${phylipFolder}"
echo "# treeFolder=${treeFolder}"
echo "# scriptsFolder=${scriptsFolder}"
echo "# isMotif=${isMotif}"
echo 

if [ "${fastaGB}" = "YES" ]; then 
	name=${name%.fasta-gb}
fi

process_command "mkdir -p ${phylipFolder}" 0 -
process_command "mkdir -p ${treeFolder}" 0 -

count=`grep -c "^>" ${inFolder}/${name}`
echo ${count}

process_command "perl \
	${scriptsFolder}/to_phylip.pl ${count} ${len} \
	${inFolder}/${name} \
	${isMotif} \
	> ${phylipFolder}/${name}.PHYLIP" 0 -

process_command "${phymlBin} \
	-i ${phylipFolder}/${name}.PHYLIP \
	-d nt -q -n 1 -m HKY85 -s BEST -o tlr \
	--no_memory_check -b ${BOOTSTRAP} --r_seed 13" 0 -

process_command "cp -u ${phylipFolder}/${name}.PHYLIP_phyml_tree.txt ${treeFolder}/${name}.phy" 0 -

process_command "bash ${scriptsFolder}/create_pdf_using_njplot.sh ${treeFolder}/${name}.phy" 0 -

