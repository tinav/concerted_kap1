#!/bin/bash

fileName=$1
echo "fileName "$fileName
#parse filename: path, first part of basename (before first dot)

dir=${fileName%/*}
echo "dir "$dir
x=${fileName##*/}
firstPart=${x%%.*}
echo "firstPart "$firstPart
baseName=${x%.*}
echo "baseName "$baseName
njplot -psonly -boot $fileName
mv $dir/$firstPart.ps $dir/$baseName.ps  
ps2pdf $dir/$baseName.ps $dir/$baseName.pdf
